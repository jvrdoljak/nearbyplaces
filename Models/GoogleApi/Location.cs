﻿namespace NearbyPlaces.Models.GoogleApi
{
    /// <summary>
    /// Google place coordinates.
    /// </summary>
    public class Location
    {
        /// <summary>
        /// Latitude.
        /// </summary>
        public double lat { get; set; }

        /// <summary>
        /// Longitude.
        /// </summary>
        public double lng { get; set; }
    }
}
