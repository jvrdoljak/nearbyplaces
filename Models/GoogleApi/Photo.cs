﻿using System.Collections.Generic;

namespace NearbyPlaces.Models.GoogleApi
{
    /// <summary>
    /// 
    /// </summary>
    public class Photo
    {
        /// <summary>
        /// 
        /// </summary>
        public int height { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<string> html_attributions { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string photo_reference { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int width { get; set; }
    }
}
