﻿using System.Collections.Generic;

namespace NearbyPlaces.Models.GoogleApi
{
    /// <summary>
    /// 
    /// </summary>
    public class OpeningHours
    {
        /// <summary>
        /// 
        /// </summary>
        public bool open_now { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<object> weekday_text { get; set; }
    }
}
