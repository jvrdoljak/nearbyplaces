﻿using System.Collections.Generic;

namespace NearbyPlaces.Models.GoogleApi
{
    /// <summary>
    /// Google places main response model.
    /// </summary>
    public class PlacesApiQueryResponse
    {
        /// <summary>
        /// 
        /// </summary>
        public List<object> html_attributions { get; set; }

        /// <summary>
        /// List of all results.
        /// </summary>
        public List<Result> results { get; set; }

        /// <summary>
        /// Http status code.
        /// </summary>
        public string status { get; set; }
    }
}

