﻿using System.Collections.Generic;

namespace NearbyPlaces.Models.GoogleApi
{
    /// <summary>
    /// Google places result.
    /// </summary>
    public class Result
    {
        /// <summary>
        /// 
        /// </summary>
        public Geometry geometry { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string icon { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public OpeningHours opening_hours { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<Photo> photos { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string place_id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public double rating { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string reference { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string scope { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<string> types { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string vicinity { get; set; }
    }
}
