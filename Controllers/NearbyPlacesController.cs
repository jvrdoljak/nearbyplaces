﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NearbyPlaces.Filter;
using NearbyPlaces.Heplers;
using NearbyPlaces.Models.GoogleApi;
using NearbyPlaces.Services.GoogleApi;
using NearbyPlaces.Services.NearbyPlaces;
using NearbyPlaces.Services.UriService;
using NearbyPlaces.ViewModels.NearbyPlaces;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace NearbyPlaces.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class NearbyPlacesController : ControllerBase
    {
        /// <summary>
        /// Configuration variable.
        /// </summary>
        private readonly IConfiguration _configuration;

        /// <summary>
        /// Nearby places service.
        /// </summary>
        private readonly INearbyPlacesService _nearbyPlacesService;

        /// <summary>
        /// 
        /// </summary>
        private readonly IGoogleApiService _googleApiService;

        /// <summary>
        /// 
        /// </summary>
        private readonly IUriService _uriService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nearbyPlacesService"></param>
        /// <param name="configuration"></param>
        /// <param name="googleApiService"></param>
        public NearbyPlacesController(
            INearbyPlacesService nearbyPlacesService, 
            IConfiguration configuration,
            IGoogleApiService googleApiService,
            IUriService uriService
            )
        {
            _configuration = configuration;
            _nearbyPlacesService = nearbyPlacesService;
            _googleApiService = googleApiService;
            _uriService = uriService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="lat"></param>
        /// <param name="lng"></param>
        /// <param name="type"></param>
        /// <param name="input"></param>
        /// <param name="sessionToken"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index([FromQuery] PaginationFilter filter, string lat, string lng, string type = null, string input = null, string sessionToken = null)
        {
            PlacesApiQueryResponse nearbyPlaces;
            HttpResponseMessage response;

            response = await _googleApiService.GetDataFromApiAsync(lat, lng);

            nearbyPlaces = JsonSerializer
                .Deserialize<PlacesApiQueryResponse>(await response.Content.ReadAsStringAsync());

            var places = _nearbyPlacesService
                .Map(nearbyPlaces.results);

            var availableTypes = _nearbyPlacesService
                .GetAllAvailableTypes(nearbyPlaces.results);

            places = _nearbyPlacesService
                .FilterPlaces(places, input == null ? null : input.Replace("%", " "), type);

            var route = Request.Path.Value;
            var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);

            var pagedData = places
               .Skip((validFilter.PageNumber - 1) * validFilter.PageSize)
               .Take(validFilter.PageSize)
               .ToList();
            var totalRecords = places.Count();
            var pagedReponse = PaginationHelper
                .CreatePagedReponse(validFilter, totalRecords, _uriService, route);

            NearbyPlacesViewModel bla = new NearbyPlacesViewModel();
            bla.pagination = pagedReponse;
            bla.nearbyPlaces = pagedData;
            bla.AvailableTypes = availableTypes;
            bla.SessionToken = sessionToken;
            
            return Ok(bla);
        }
    }
}
