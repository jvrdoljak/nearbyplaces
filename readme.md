﻿# Aircash zadatak

* Treba napraviti jednu web stranicu koja će prikazati sve lokale (urede, kafiće, frizerske salone...) u krugu od 1 km od zadane lokacije
* Informacije o lokalima se može dobiti s bilo kojeg javnog servisa (npr Foursquare-a ili Google Places). 
 
## Također treba:
* Omogućiti filter po kategoriji i pretragu liste.
* Lista mora imati:
	* paginaciju, 
	* filter za vrstu lokala, 
	* svi requesti prema serverskoj strani aplikacije moraju biti asinkroni (Ajax).

### Ostalo prepuštamo tvojoj mašti pa se slobodno izrazi i doprinesi rješenju svojim idejama! Molio bih te samo da rješenje bude u ASP.NET-u, što se tiče frontenda može bilo što, extra plus za Angular. Kontekst rješenja bi trebao biti veliki sustav pa bi trebalo pripaziti na strukturu rješenja projekta. 
