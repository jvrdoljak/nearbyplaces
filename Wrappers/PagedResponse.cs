﻿using NearbyPlaces.Wrappers;
using System;
using System.Collections.Generic;

namespace NearbyPlaces.Wrapper
{
    /// <summary>
    /// Pagination wrapper.
    /// </summary>
    public class PagedResponse : Response
    {
        /// <summary>
        /// Number of pages to render.
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// Size of each size.
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// Url to first page.
        /// </summary>
        public Uri FirstPage { get; set; }

        /// <summary>
        /// Url to last page.
        /// </summary>
        public Uri LastPage { get; set; }

        /// <summary>
        /// Number of total pages.
        /// </summary>
        public int TotalPages { get; set; }

        /// <summary>
        /// Number of total records.
        /// </summary>
        public int TotalRecords { get; set; }

        /// <summary>
        /// Next page url.
        /// </summary>
        public Uri NextPage { get; set; }

        /// <summary>
        /// Previuous page url.
        /// </summary>
        public Uri PreviousPage { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        public PagedResponse(int pageNumber, int pageSize)
        {
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
            this.Message = null;
            this.Succeeded = true;
            this.Errors = null;
        }
    }
}
