﻿using NearbyPlaces.Filter;

namespace NearbyPlaces.Services.UriService
{
    /// <summary>
    /// 
    /// </summary>
    public interface IUriService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="route"></param>
        /// <returns></returns>
        public System.Uri GetPageUri(PaginationFilter filter, string route);
    }
}
