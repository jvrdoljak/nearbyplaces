﻿using NearbyPlaces.Models.GoogleApi;
using NearbyPlaces.ViewModels.NearbyPlaces;
using System.Collections.Generic;
using System.Linq;

namespace NearbyPlaces.Services.NearbyPlaces
{
    public class NearbyPlacesService : INearbyPlacesService
    {
        public List<PlaceViewModel> Map(List<Result> results)
        {
            var nearbyPlacesViewModels = new List<PlaceViewModel>();

            foreach (var result in results)
            {
                var nearbyPlacesViewModel = new PlaceViewModel();

                nearbyPlacesViewModel.name = result.name ?? "No name.";
                nearbyPlacesViewModel.icon = result.icon ?? "";
                nearbyPlacesViewModel.rating = result.rating;
                nearbyPlacesViewModel.types = result.types;
                nearbyPlacesViewModel.vicinity = result.vicinity ?? "No vicinity for display.";

                nearbyPlacesViewModels.Add(nearbyPlacesViewModel);
            }

            return nearbyPlacesViewModels;
        }

        public List<string> GetAllAvailableTypes(List<Result> results)
        {
            List<string> availableTypes = new List<string>();

            foreach(var result in results)
            {
                result.types.ForEach(a => availableTypes = AddTypeToAvailableTypes(availableTypes, a));
            }

            availableTypes.Sort();

            // add all as default type
            availableTypes.Insert(0, "all");

            return availableTypes;
        }

        public List<PlaceViewModel> FilterPlaces(List<PlaceViewModel> list, string input, string type)
        {

            if (type != null && type.Length > 0 && !type.ToLower().Equals("all"))
            {
                list = list.FindAll(a => a.types.Contains(type));
            }

            if (input != null && input.Length > 0)
            {
                //TODO: Implement random word order filtering.
                list =  list.Where(a => a.name.ToLower().Contains(input.ToLower()) || a.vicinity.ToLower().Contains(input.ToLower())).ToList();
            }

            return list;
        }

        /// <summary>
        /// Add type to available types if it not exist in list.
        /// </summary>
        /// <param name="types"></param>
        /// <param name="type"></param>
        /// <returns>Available types.</returns>
        private List<string> AddTypeToAvailableTypes(List<string> types, string type)
        {
            if (!IsTypeExistInList(types, type))
            {
                types.Add(type);
            }
            return types;
        }

        /// <summary>
        /// If type still exist in available types.
        /// </summary>
        /// <param name="types"></param>
        /// <param name="type"></param>
        /// <returns>True if exist, false if don't exist.</returns>
        private bool IsTypeExistInList(List<string> types, string type)
        {
            if(types.Contains(type))
            {
                return true;
            } else
            {
                return false;
            }
        }
    }
}
