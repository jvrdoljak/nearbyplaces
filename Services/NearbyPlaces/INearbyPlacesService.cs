﻿using NearbyPlaces.Models.GoogleApi;
using NearbyPlaces.ViewModels.NearbyPlaces;
using System.Collections.Generic;

namespace NearbyPlaces.Services.NearbyPlaces
{
    /// <summary>
    /// 
    /// </summary>
    public interface INearbyPlacesService
    {
        /// <summary>
        /// Map places.
        /// </summary>
        /// <param name="results"></param>
        /// <returns></returns>
        public List<PlaceViewModel> Map(List<Result> results);

        /// <summary>
        /// Get all available Google Place types which are in api response.
        /// </summary>
        /// <param name="results"></param>
        /// <returns>All available types.</returns>
        public List<string> GetAllAvailableTypes(List<Result> results);

        /// <summary>
        /// Function filters places by finding text input in name or vicinity and by type.
        /// </summary>
        /// <param name="list"></param>
        /// <param name="input"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public List<PlaceViewModel> FilterPlaces(List<PlaceViewModel> list, string input, string type);
    }
}
