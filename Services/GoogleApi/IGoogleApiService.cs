﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace NearbyPlaces.Services.GoogleApi
{
    /// <summary>
    /// 
    /// </summary>
    public interface IGoogleApiService
    {
        /// <summary>
        /// Returns data from google api places.
        /// </summary>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="sessionToken">Random string token for google api.</param>
        /// <returns></returns>
        public Task<HttpResponseMessage> GetDataFromApiAsync(string latitude, string longitude, string sessionToken = null);
    }
}
