﻿using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace NearbyPlaces.Services.GoogleApi
{
    public class GoogleApiService : IGoogleApiService
    {
        private readonly IConfiguration _configuration;
        private readonly string _googlePlacesApiKey;
        private readonly string _radius;
        private readonly string _baseApiUrl;

        public GoogleApiService(IConfiguration configuration)
        {
            _configuration = configuration;
            _googlePlacesApiKey = _configuration.GetSection("GooglePlacesApiKey").Value;
            _radius = _configuration.GetSection("DefaultGooglePlacesRadius").Value;
            _baseApiUrl = _configuration.GetSection("BaseApiUrl").Value;
        }

        public async Task<HttpResponseMessage> GetDataFromApiAsync(string latitude, string longitude, string sessionToken = null)
        {
            HttpClient client = new HttpClient();
            HttpResponseMessage response;
            HttpRequestMessage request;

            var requestUri = QueryHelpers.AddQueryString(_baseApiUrl, MakeQueryString(latitude, longitude, sessionToken));

            request = new HttpRequestMessage(HttpMethod.Get, requestUri);
            client.BaseAddress = new Uri(_baseApiUrl);

            response = await client.SendAsync(request);
            response.EnsureSuccessStatusCode();

            return response;
        }

        private List<KeyValuePair<string, string>> MakeQueryString(string latitude, string longitude, string sessionToken = null)
        {
            List<KeyValuePair<string, string>> queryString = new List<KeyValuePair<string, string>>();
            queryString.Add(new KeyValuePair<string, string>("key", _googlePlacesApiKey));
            queryString.Add(new KeyValuePair<string, string>("location", latitude + "," + longitude));
            queryString.Add(new KeyValuePair<string, string>("radius", _radius));
            if (sessionToken != null && sessionToken.Length > 0)
            {
                queryString.Add(new KeyValuePair<string, string>("sessiontoken", sessionToken));
            }

            return queryString;
        }

    }
}
