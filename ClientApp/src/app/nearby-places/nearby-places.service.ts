import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest } from '@angular/common/http';
import { NearbyPlaces } from '../shared/models/nearby-places/nearby-places';

@Injectable({
  providedIn: 'root'
})

export class NearbyPlacesService {
  private readonly nearbyPlacesBaseUrl: string;
  private readonly REST_API_SERVER = "https://localhost:44377";

  constructor(private httpClient: HttpClient) {
    this.nearbyPlacesBaseUrl = this.REST_API_SERVER + '/api/nearbyplaces?';
  }

  public getPlaces(lat: string, lng: string, input?: string, type?: string, pageNumber?: number, pageSize?: number) {
    let url = this.nearbyPlacesBaseUrl;
    url += 'lat=' + lat + '&lng=' + lng;
    if (pageNumber) {
      url += '&pageNumber=' + pageNumber;
    }
    if (pageSize) {
      url += '&pageSize=' + pageSize;
    }
    if (input && input.length > 0) {
      url += '&input=' + input;
    }
    if (type && type.length > 0) {
      url += '&type=' + type;
    }
    return this.httpClient.get<NearbyPlaces>(url);
  }
}
