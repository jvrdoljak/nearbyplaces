import { Component, OnInit } from '@angular/core';
import { NearbyPlacesService } from './nearby-places.service';
import { Location } from '../shared/models/nearby-places/location';
import { NearbyPlaces } from '../shared/models/nearby-places/nearby-places';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { NgModule } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-nearby-places',
  templateUrl: './nearby-places.component.html',
  styleUrls: ['./nearby-places.component.css']
})

export class NearbyPlacesComponent implements OnInit {
  public isLoading = false;
  public nearbyPlaces: NearbyPlaces;
  public types: string[];
  public regexUnderline = /_/ig;
  public regexSpace = / /ig;
  public selectedType: string;
  private location: Location;
  public inputText: string;
  that = this;

  displayedColumns: string[] = ['icon', 'name', 'vicinity', 'rating', 'types'];

  length = 0;
  pageSize = 5;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  previousPageIndex!: number;
  pageIndex: number = 1;
  pageEvent!: PageEvent;

  constructor(private nearbyPlacesService: NearbyPlacesService) {
    this.location = new Location();
    this.nearbyPlaces = new NearbyPlaces();
    this.types = [];
    this.selectedType = 'all';
    this.inputText = '';
    this.that = this;
  }

  ngOnInit() {
    var that = this;
    this.getCurrentLocation().then(function (value) {
      that.location.latitude = value.latitude;
      that.location.longitude = value.longitude;
      that.getNearbyPlaces();
    }).catch((a) => {
      console.log(a);
    });
  }
  setPageSizeOptions(setPageSizeOptionsInput: string) {
    if (setPageSizeOptionsInput) {
      this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
    }
  }

  public getNearbyPlaces() {
    this.isLoading = true;
    this.nearbyPlacesService
      .getPlaces(
        this.location.latitude,
        this.location.longitude,
        this.inputText.replace(this.regexSpace, "%"),
        this.selectedType,
        this.pageIndex,
        this.pageSize
      )
      .subscribe((data: NearbyPlaces) => {
        this.nearbyPlaces = data;
        this.length = data.pagination.totalRecords;
        this.pageIndex = 1;
        this.isLoading = false;
      });
  }

  public getPlaces() {
    this.isLoading = true;
    this.nearbyPlacesService.getPlaces(
      this.location.latitude,
      this.location.longitude,
      this.inputText.replace(this.regexSpace, "%"),
      this.selectedType,
      this.pageIndex,
      this.pageSize
    )
      .subscribe((data: NearbyPlaces) => {
        this.nearbyPlaces.nearbyPlaces = data.nearbyPlaces;
      this.length = data.pagination.totalRecords;
      this.pageIndex = 1;
      this.isLoading = false;
    });
  }

  handlePageEvent(event: any) {
    this.pageIndex = event.pageIndex + 1;
    this.getPlaces();
  }

  onSubmitTypes(selectedType: string): void {
    this.selectedType = selectedType;
    this.getPlaces();
  }

  onSubmitSearch(inputText: string): void {
    this.getPlaces();
  }

  private getCurrentLocation(this: any): Promise<Location> {
    var location = new Location();
    return new Promise<Location>(function (resolve, reject) {
      navigator.geolocation.getCurrentPosition(function (pos) {
        location.latitude = pos.coords.latitude.toString()
        location.longitude = pos.coords.longitude.toString()
        resolve(location);
      })
    });
  }
}
