import { Place } from "./place";

export class Pagination {
  public pageNumber: number;
  public pageSize!: number;
  public firstPage: string;
  public lastPage: string;
  public totalPages: number;
  public totalRecords: number;
  public nextPage: string;
  public previousPage: string;
  public succeeded: boolean;
  public errors: string;
  public message: string;

  constructor() {
    this.pageNumber = 1;
    this.firstPage = '';
    this.lastPage = '';
    this.totalPages = 0;
    this.totalRecords = 0;
    this.nextPage = '';
    this.previousPage = '';
    this.succeeded = false;
    this.errors = '';
    this.message = '';
  }
}
