export class Location {
  public latitude: string;
  public longitude: string;

  constructor() {
    this.latitude = '';
    this.longitude = '';
  }
}
