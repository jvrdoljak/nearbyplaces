export class Place {
  public name: string;
  public icon: string;
  public vicinity: string;
  public rating: number;
  public types: string[];

  constructor() {
    this.name = '';
    this.icon = '';
    this.vicinity = '';
    this.rating = 0;
    this.types = [];
  }
}
