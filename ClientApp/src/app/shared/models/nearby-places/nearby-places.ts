import { Pagination } from './pagination';
import { Place } from "./place";

export class NearbyPlaces {
  public nearbyPlaces: Place[];
  public availableTypes: string[];
  public sessionToken: string;
  public pagination!: Pagination; 
  constructor() {
    this.nearbyPlaces = [];
    this.availableTypes = [];
    this.sessionToken = '';
  }
}
