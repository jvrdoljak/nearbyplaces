﻿using System.Collections.Generic;

namespace NearbyPlaces.ViewModels.NearbyPlaces
{
    /// <summary>
    /// View model for data rendering.
    /// </summary>
    public class PlaceViewModel
    {
        /// <summary>
        /// Google place name.
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// Google place icon path.
        /// </summary>
        public string icon { get; set; }

        /// <summary>
        /// Google place vicinity.
        /// </summary>
        public string vicinity { get; set; }

        /// <summary>
        /// Google place rating.
        /// </summary>
        public double rating { get; set; }

        /// <summary>
        /// Google place types.
        /// </summary>
        public ICollection<string> types { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public PlaceViewModel()
        {
            types = new List<string>();
        }
    }
}
