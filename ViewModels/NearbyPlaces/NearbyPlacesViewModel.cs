﻿using NearbyPlaces.Wrapper;
using System.Collections.Generic;

namespace NearbyPlaces.ViewModels.NearbyPlaces
{
    /// <summary>
    /// Main reponse view model.
    /// </summary>
    public class NearbyPlacesViewModel
    {
        /// <summary>
        /// All available types that are in google places response.
        /// </summary>
        public List<string> AvailableTypes { get; set; }

        /// <summary>
        /// Session token for google places api.
        /// </summary>
        public string SessionToken { get; set; }
        
        /// <summary>
        /// Session token for google places api.
        /// </summary>
        public PagedResponse pagination { get; set; }
        
        /// <summary>
        /// Data to render.
        /// </summary>
        public List<PlaceViewModel> nearbyPlaces { get; set; }
    }
}
